﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Diagnostics;

namespace Voice_Recognition
{
    public partial class Form1 : Form
    {

        SpeechRecognitionEngine _recognition_engine = new SpeechRecognitionEngine();
        SpeechRecognitionEngine _starting_to_listen = new SpeechRecognitionEngine();
        SpeechSynthesizer _speech = new SpeechSynthesizer();

        int RecognitionTimeout = 0;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Choices commands = new Choices();
            commands.Add(
                new string[] { "what is your name", "time", "how are you",
                "search","open facebook", "open youtube", "location"}
                );
            GrammarBuilder grammerBuilder = new GrammarBuilder();
            grammerBuilder.Append(commands);
            Grammar grammer = new Grammar(grammerBuilder);



            Choices wake_up_commands = new Choices();
            wake_up_commands.Add(new string[] { "stop listening",  "wake up"});
            GrammarBuilder wake_up_grammerBuilder = new GrammarBuilder();
            wake_up_grammerBuilder.Append(commands);
            Grammar wake_up_grammer = new Grammar(wake_up_grammerBuilder);


            _recognition_engine.SetInputToDefaultAudioDevice(); //mora da postoji audio device na uredjaju na kome se izvrsava aplikacija.
            _recognition_engine.LoadGrammarAsync(grammer);
            _recognition_engine.SpeechRecognized += SpeechRecognized;
            _recognition_engine.SpeechDetected += 
                    new EventHandler<SpeechDetectedEventArgs>(RecognizerSpeachRecognized);

            _starting_to_listen.SetInputToDefaultAudioDevice();
            _starting_to_listen.LoadGrammarAsync(wake_up_grammer);
            _starting_to_listen.SpeechRecognized += 
                    new EventHandler<SpeechRecognizedEventArgs>(StartListening);
        }

        private void StartListening(object sender, SpeechRecognizedEventArgs e)
        {
           switch(e.Result.Text)
            {
                case "wake up":
                    _starting_to_listen.RecognizeAsyncCancel();
                    _speech.SpeakAsync("Yes i am here");
                    _recognition_engine.RecognizeAsync(RecognizeMode.Multiple);
                    txtConsole.Clear();
                    break;
            }
        }

        private void RecognizerSpeachRecognized(object sender, SpeechDetectedEventArgs e)
        {
            RecognitionTimeout = 0;
        }

        private void btnEnable_Click(object sender, EventArgs e)
        {
            _recognition_engine.RecognizeAsync(RecognizeMode.Multiple);
            btnDisable.Enabled = true;
            btnEnable.Enabled = false;
        }

        void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            switch(e.Result.Text)
            {
                case "what is your name":
                    _speech.SpeakAsync("My name is Alexa");
                    break;
                case "how are you":
                    _speech.SpeakAsync("Thanks for asking, i am good");
                    break;
                case "time":
                    string date = DateTime.Now.ToString("h mm tt");
                    _speech.SpeakAsync(date);
                    txtConsole.Text += date + Environment.NewLine;
                    break;
                case "search":
                    _speech.SpeakAsync("What do you want to searh for?");
                    txtConsole.Text = "Searching Google" + Environment.NewLine;
                    string url = "https://google.com/search?q=";
                    Process.Start(url);
                    break;
                case "location":
                    _speech.SpeakAsync("Here is your location");
                    txtConsole.Text += "Searching your location" + Environment.NewLine;
                    string urlLocation = "https://www.google.com/maps/@43.3313865,21.8922878,20.33z";
                    Process.Start(urlLocation);
                    break;
                case "open facebook":
                    _speech.SpeakAsync("Opening Facebook");
                    txtConsole.Text += "Open facebook" + Environment.NewLine;
                    string facebook = "https://www.facebook.com/";
                    Process.Start(facebook);
                    break;
                case "open youtube":
                    _speech.SpeakAsync("Opening Youtube");
                    txtConsole.Text += "Open youtube" + Environment.NewLine;
                    string youtube = "https://www.youtube.com/";
                    Process.Start(youtube);
                    break;
                case "stop listening":
                    _speech.SpeakAsync("If you need me just ask");
                    _recognition_engine.RecognizeAsyncCancel();
                    _starting_to_listen.RecognizeAsync(RecognizeMode.Multiple);
                    break;
                default:
                    _speech.SpeakAsync("I did not recognize that");
                    break;

            }
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            _recognition_engine.RecognizeAsyncStop();
            txtConsole.Clear();
            btnEnable.Enabled = true;
            btnDisable.Enabled = false;
        }

        private void timerSpeech_Tick(object sender, EventArgs e)
        {
            if (RecognitionTimeout == 10) _recognition_engine.RecognizeAsyncCancel();
            else if (RecognitionTimeout == 11)
            {
                timerSpeech.Stop();
                _starting_to_listen.RecognizeAsync(RecognizeMode.Multiple);
                txtConsole.Clear();
                RecognitionTimeout = 0;
            }

        }
    }
}
